# EDOM - Project Research Topic

**Disclaimer**: The content present on this report is based of a conference paper written by the following authors: Renato Oliveira, David Pereira, Cláudio Maia and Pedro José Santos. Such paper can be found  [here](https://www.cister.isep.ipp.pt/docs/a_domain_specific_language_for_automotive_systems_integration/1565/view.pdf). 

## MPS   

### Introduction

MPS is an intellectual property of JetBrains and is a metaprogramming system and also known a
language workbench that allows the development of DSLs,
simultaneously creating models for such DSLs, generating
code, perform model verification, define annotations and create
documentation. MPS relies on a projectional editor, meaning
that the Abstract Syntax Tree (AST) is changed after every
input instead of having a parser that transforms a character
sequence in that same tree. This brings some advantages,
namely, language modularity and flexible notation capabilities.

It comes with an editor which allows visualization of the AST in a user-friendly way and provides the means for effective editing. For classical textual languages, the editor should provide the user with the illusion of editing text in a text-like manner, for graphical notations, on the other hand, the editor should take on the habits of a well-behaved diagramming editor. Additionally when creating a language, it is possible to define the rules for code editing and rendering. It allows specification of the language type-system and constraints. This allows MPS to check program code on the fly, and so makes programming with the new language easy and less error-prone.

### Real case scenario

Developing complex CPS applications for the automotive
domain is a complex task, due to the criticality inherent to this
domain and the need for increased functionality, safety, and
security. A considerable part of the development effort relies
on the integration of third-party and vendor specific software
components, thus involving the usage of plethora of different
technologies, typically developed in isolation (and by different
entities) and, later in the process, integration and verification
efforts that delay the time to market of new solutions, and
higher production costs.

 The different technologies typically
involved in the development of automotive applications range
from distinct modeling frameworks, code editors and programming languages, development frameworks, operating systems,
COTS platforms, support for virtualization via hypervisors,
among others. Such diversity of technologies leads to an
integration that can be very complex, tedious, error prone, and
time-, money- and effort-consuming [1].

As the complexity of these CPSs increases, our inability
to rigorously capture the interactions between the physical
and the computation components paves the way to software
errors that can lead to serious vulnerabilities during operations.
Ultimately, systems become unsafe, with disastrous failures
that could not have been properly identified during the several
stages of the application’s development cycle [2].

In order to properly address the complexity of the design
and development process, we propose the development of a
DSL that aims to provide a quasi-automatic integration process
between the different entities or components of a CPS application, abstracting away technical details of these components
and enable rigorous methods of verification, customization,
and deployment. To this end, the DSL provides different views
of the system, where properties or constraints imposed in a
view propagate to other views, increasing the set of systemwide properties that need to be ensured correct in order for
the system to be considered safe.

 By providing distinct views,
our envisioned solution allows different system intervenients to
seamlessly interact during the development process, simultaneously ensuring that the underlying infrastructure used for this
interaction is verified, and that CPS applications and systems
can be easily built, customized and deployed.

### Hypothetical implementation of the real scenario

With the ever growing demand for cars to be equipped
with more advanced, safe, and secure Advanced DriverAssistance Systems (ADAS), whose development costs and
time-to-market have reduced, we foresee that many CPS
applications will make use of high-performance computing
hardware platforms equipped with several, potentially heterogeneous computing cores, as well as with GPUs specially
tailored to improve the performance and energy efficiency of
advanced computation features, like neural networks or image
processing algorithms.
Also importantly, single OS solutions are also becoming
quite limited, as complex CPS applications for the automotive
sector clearly require higher degrees of isolation between components of the system since these components have different
levels of criticality and security.

 Therefore, hypervisors that
are able to manage several OSs within the same platform
are gaining relevance, as they naturally enforce temporal
and spacial isolation among components, and thus allow
interference mitigation and provide a natural way to have
components executing in an OS environment that provides
support for predictable computation and operation over the
physical environment (e.g., real-time operating systems satisfying AUTOSAR requirements), and for these to send data for
being processed in standard Linux OSs where frameworks like
the Robot Operating System (ROS) are used for coordination
of components that are responsible for providing the ”intelligence” required for vehicles with advanced ADAS features,
for example.


In particular, we
consider a CPS application comprised of three partitions (each
corresponding to a guest operating system, two of which are
standard Linux and the remaining one hosting a RTOS), one
hypervisor managing those partitions, all of which running on
a high-performance computing platform Nvidia Jetson TX2.
One of the Linux partitions will serve as the interface with
the driver, the second Linux partition hosts ROS as its most
outstanding application, and the RTOS running in the third
partition is assumed to be in charge of managing the sensing
of, and the actuation over the physical instruments of the
vehicle. The high-level of this CPS system’s architecture is
illustrated in Figure 1.

![figure_one](images/cps_application.png)

From the architecture of the above described example CPS
application, we can immediately identify four different views
of that application: 

* Platform view (PV), where the
hardware platform is chosen and the set of resources that have
to be used in the application;
* Hypervisor view (HV),
where one needs to configure how many partitions have to
be defined and how they communicate with each other (if
they communicate at all); 
* Operating system view (OV)
where the OS to be deployed in each partition is selected, and
customized to reduce resource usage and potential interference
with the applications that will be running on them; 
* Application view (AV), where the actual computational
components responsible for the functionality of the system
are identified, their interactions are defined, and from which
verification conditions can be derived.

Each of these views contains specific information which
is necessary to be known by other views in order to establish
strict boundaries on the support that each view can have or can
provide to the other view to which it is directly connected.
For instance, the HV must provide information to the OV
indicating that only a subset of operating systems is supported,
which in turn impacts on the set of components that can be
deployed in each of the operating systems that may be chosen.

**In order to not saturate the report, the process performed to generate the DSL will be shown in the demo.**

The next sections cover some specific aspects related to the MPS tool [3].


### Metamodeling

Since MPS frees definition of a grammar for the intended languages, there must exist different ways to specify the structure of a given language. This is where the Structure Language comes in handy. It gives all the means to define the language structure. When coding in MPS we are effectively building the AST directly, so the structure of the language needs to specify the elements, the bricks used to build the AST.

The bricks are called **Concepts** and the Structure Language exposes concepts and concept interfaces as well as their members: properties, references, and children.

![](./images/metamodeling1.png)

Since the Structure Language may sometimes be insufficient to express advanced constraints on the language structure. The **Constraints** aspect gives a way to define such additional constraints. 

##### Can be child/parent/ancestor/root

These are the first knobs to turn when defining constraints for a concept. They determine whether instances of this concept can be hooked as children (parents, ancestors) nodes of other nodes or root nodes in models. You specify them as boolean-returning closures,which MPS invokes each time when evaluating allowed position for a node in the AST.

![](./images/constraint1.png)

##### Property constraints

Technically speaking, "pure" concept properties are not properties in its original meaning, but only public fields. Property constraints allows to make them real properties. Using these constraints, the behavior of concept's properties can be customized. Each property constraint is applied to a single specified property.

### DSL


The DSL is the aggregation of the various concepts previously defined and the DSL syntax is the totality of the concepts editor's aspects. Each concept has an editor's aspect which has the syntax and the semantic rules.

![DSL](./images/dsl.png)

An editor for a node serves as its view as well as its controller. An editor displays the node and lets the user modify, replace, delete it and so on. Nodes of different concepts have different editors. 

In MPS, an editor consists of cells, which themselves contain other cells, some text, or a UI component. Each editor has its concept for which it is specified. A concept may have no more than one editor declaration (or can have none). If a concept does not have an editor declaration, its instances will be edited with an editor for the concept's nearest ancestor that has an editor declaration.

To describe an editor for a certain concept (i.e. which cells have to appear in an editor for nodes of this concept). The description of an editor consists of descriptions for cells it holds. Such descriptions are called "cell models." 

### Model to model transformations

MPS follows the model-to-model transformation approach. The MPS generator specifies translation of constructions encoded in the input language into constructions encoded in the output language. The process of model-to-model transformation may involve many intermediate models and ultimately results in sn output model, in which all constructions are in a language whose semantics are already defined elsewhere.

The transformation is described by means of templates. Templates are written using the output language and so can be edited with the same cell editor that would normally be used to write 'regular code' in that language. Therefore, without any additional effort the 'template editor' has the same level of tooling support right away - syntax/error highlighting, auto-completion, etc. The templates are then parametrized by referencing into the input model.

Applicability of each transformation is defined by generator rules.
There are six types of generator rules:

* conditional root rule

* root mapping rule

* weaving rule

* reduction rule

* pattern rule

* abandon root rule

* drop attribute rule 

![model_to_model](./images/modeltomodel.png)

### Model to text (code generation)

Model to text and model to model go hand in hand (meaning they have a lot of similarities) such that the TextGen language aspect defines a model to text transformation. It comes in handy each time it is need to convert models into the text form directly. The language contains constructs to print out text, transform nodes into text values and give the output some reasonable layout. Below is shown an example of a transformation using the language aspect TextGen.

    text gen component for concept Constants {
        file name : Constants
        extension : <no extension>
        encoding : utf-8
        (context, buffer, node)->void {
            append ${node.name} \n ;
            with indent {
                node.constants.forEach({~constant =>
                    indent buffer ;
                    append ${constant.name} {=} ${constant.initializer} \n ;
                });
            }
        }
    }

### Migrations support

After a language has been published and users have started using it, the language authors have to be careful with further changes to the language definition. In particular, removing concepts or adding and removing properties, children and references to concepts will introduce incompatibilities between the previous and the next language version. This impacts the users of the language if they update to the next language version, since they may discover that their model no longer matches the language definitions and get appropriate errors reported from their models.

MPS tracks versions of languages used in projects and provides automatic migrations to upgrade the usages of a language to the most recent versions. The language designers can create maintenance "migration" code to run automatically against the user code and thus change the user's code so that it complies with the changes made to the language definition. This is called language migration.




### References

[1] R. Land and I. Crnkovic, “Software systems integration and architectural
analysis - a case study,” 10 2003, pp. 338– 347.

[2] A. Sangiovanni-Vincentelli, W. Damm, and R. Passerone, “Taming dr.
frankenstein: Contract-based design for cyber-physical systems,” European journal of control, vol. 18, no. 3, pp. 217–238, 2012.

[3] JetBrains, “MPS User's Guide” 2019. [Online]. Available: https://www.jetbrains.com/help/mps/mps-user-s-guide.html


