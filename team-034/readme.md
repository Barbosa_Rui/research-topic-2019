# EDOM - Project Research Topic

# EMF FORMS
## ABOUT & SMALL EXAMPLE ILLUSTRATING

EMF Forms - a subcomponent of EMF Client Platform framework - focus on the development of UIs based on the creation of a descriptive model of the UI, instead of creating UI related code.

The purpose of this framework is to ease the development and layout definition of form-based UI’s that allow browsing and data management (CRUD) from an user standpoint.

The main goals of using EMF are:

- Providing a new way to develop UI’s: instead of coding form-based layouts, EMF Forms allow us to describe the UI with a simple model
- Allowing the iterative refining of the UI's
- The UI description is interpreted by a rendering engine, so it’s possible to switch between the UI technology stack to Swing, SWT or web, just by replacing the renderer

----

EMF Forms extrapolate the desired input type (i.e.: checkbox, textbox, ...) from the designated EType of a given field in the model. An EString-type of field would generate a textbox, for example.

Additionally, EMF Forms aim at removing the responsibility of input-processing from the UI's code - this responsibility transitions into the modeled data entities.

This contributes to:

- Reduction in effort in developing UI's - the developers do not need to focus (for the most part) in input validation
- Model reusability - either for a new UI or for other business purposes, by associating the data input management to a model allows for a smoother transition between different UI's
- Crease the lines between what responsibilities belong exclusively to the UI/frontend and the rest of the application

In some cases, parts of the business logic can also be mapped to the created models.

Based on the characteristics of the EMF Forms, we can split them into 4 groups:

- Form-Based CRUD UI’s:
	- Ready-to-use widgets
	- Based on the Data Model
	- Embeddable anywhere

![form_based](crud.png)


- Design of the UI’s:
	- Describe the layout
	- No layout coding required
	- Integrate custom controls
	- Design according to the desired style


- Customizing:
	- Adapt existing UI renderers
	- UI renderers for multiple platforms (HTML5, mobile, …)

![customizing](customize.png)

- Tooling:
	- Declarative form editor
	- Form preview
	- Maintain or easily change

![tooling](tooling.png)

- Convention over Configuration:
	- No initial coding
	- No code generation


Despite all of the characteristics stated above, there are some drawbacks to the EMF Forms - these derive directly from their association with Model Driven Engineering.

In their article "Exploiting Model Driving Technology: a Tale of Two Startups"[3], Tony Clark and Pierre-Alain Muller - two investigators from the Aston University and the University of Haute-Alsace, respectively - place two startups which ventured into the realm of MDE (not necessarily EMF Forms) under the scope. This case study shows how the term 'easy to maintain' can be ambiguous.

This case-study shows that an 'easy to maintain' (development-wise) solution may not be that 'easy to maintain' (economically-wise), which is where these two startups faltered.

While the development process was not necessarily complex, living up to the standards of MDE, the number of professionals that work in the area is still reduced, mainly caused by the fact that MDE is still not in mainstream use in the industry, leading not only to increased difficulty in finding professionals, but also to the demand of increased salaries.

Additionally, it is evident the difficulty in exploring this approach in a business model.

----

## Scenario related to MDE

![scenario](mdeEmf.png)

Starting with a data model, the idea is to map those properties into a UI (there is a binding between the data model and the form-based UI).

The view model will define where to display which properties from the data model and the result of that, will be a user interface. So, up until this point, the content representation is restricted to what is provided as preview by the IDE.

While working with MDE, EMF Forms double-dip on the model reusability philosophy - while one is meant to provide reusable models, the other one 'feeds' one them.

## Analysis and possible alternatives

![analysis1](an1.png)

In figure 5, two alternative source models (S1 and S2) are created for the requirements (R).

The distinction between S1 and S2 can be originated from different styles of modeling.

The transformation models, TM1 and TM2 can be identical or different, depending on the differences in S1 and S2.

![analysis2](an2.png)

Figure 6 shows another possibility for generating alternative solutions.

A single source model (S) can be transformed into 2 different target models (T1 and T2) by using different transformation models (TM1 and TM2). T1 and T2 can differ in functional requirements, for example.


# JSON FORMS

## ABOUT & SMALL EXAMPLE ILLUSTRATING

JSON Forms is a declarative framework for efficiently building form-based web UI’s - these leverage of JSON's goals of receiving (user-input), modifying and displaying data. In its' turn, JSON Forms are usually embedded within the application.

JSON Forms utilize the capabilities of JSON and JSON schema and provides a simple and declarative way of describing forms. Forms are then rendered with a UI library of a framework (e.g.: React or Angular).

Any UI is defined by using 2 schemas:
- The JSON schema defines the underlying data to be shown in the UI, like objects, properties and their types
- The UI schema defines how this data is rendered as a form: the order of controls, their visibility and the layout

![json_forms](example.png)

## INTEGRATION OF EMF FORMS WITH JSON FORMS

![emf_json](integration.png)

For an actual web application, there is one renderer for AngularJS which is a Javascript framework to program simple page applications - this renderer can display the same forms that were modeled with EMF Forms in the web. It's name, JSON Form.

In order to actually implement the EMF Forms UI transformation, it is necessary to convert the EMF Forms into JSON Forms. This transformation can be done using Eclipse.

This transformation traverses all EStructuralFeatures of the EClass objects found on the EMF Forms, and then generates a property for each one of them on the JSON Form. 

Since JSON Schema uses a simpler set of attributes (number, integer, string and boolean), the Ecore datatypes are mapped to these types. This can be a limitation of this association between EMF Forms and JSON Forms, but we'll discuss it later.

### **How to sustain a workflow between EMF Forms and JSON Forms**

Maintaining two completely different technology stacks, Eclipse + EMFForms on the one side, and AngularJS + JSON Forms on the other, requires a defined process.

One way, and in the opinion of the group, the best way to keep an organized workflow is to only update the EMF Forms, and generate the JSON Forms whenever is necessary. This way, after any alteration made on the EMF Forms, the JSON Forms are generated and, if correctly saved, the only thing needed to do to keep the frontend application up-to-date is to re-run the project, or reload it.

### **Limitations**

As stated above, there are some limitations to this integration. One of them, already identified, is the limitation of the datatypes available on JSON Forms. On more basic projects this might not be that important, since, in principle, it will use the basic datatypes. However, on more complex projects, this might be tricky to handle.

Another limitation is that, per type, only a single reference is considered during the export. For example, when there are multiple references with the same type, only one will be considered during the export. This is a major limitation, and it's complicated to work around it.

A final limitation is that some layouts and widgets are still not available - this is not as major as the previous limitations, since it is easier to work around this, since it is only a view limitation.

## SCENARIO RELATING TO MDE

![json_mde](mdejson.png)

So, firstly, the view model is exported to a JSON Form, turning them into a simple angular application.

The JSON Forms can work without EMF Forms. JSON Forms are like a part of the tooling of EMF Forms and the exporter will create both the JSON and UI *schemata*.

---

# Bibliography
[1] https://www.eclipse.org/ecp/emfforms/index.html

[2] https://jsonforms.io

[3] https://hal.archives-ouvertes.fr/hal-00851977/document

[4] https://eclipsesource.com/blogs/tutorials/emf-forms-and-json-forms-integration-guide