# Epsilon #

## Introdução ##

Este documento tem como objetivo a apresentação de uma 
tecnologia com um conjunto de linguagens de programação 
integradas para gerir modelos EMF e outro tipos de modelos denominada **EPSILON**. 
A escolha deste tema para a realização do projeto teórico, na unidade curricular EDOM, 
foi porque após uma pesquisa bibliográfica o grupo constatou que esta tecnologia contempla 
todas as fases desde a conceção do modelo até ao código final permitindo assim um 
termo de comparação com as tecnologias usadas durante o semestre de EDOM.

## EPSILON ##
O Epsilon fornece uma arquitetura em camadas 
que permite gerar modelos para tarefas específicas, como tarefas de transformação, validação, 
comparação e refactoring de modelos. Para habilitar as linguagens para gerar modelos, 
o Epsilon fornece uma camada de abstração chamada 
EMC (Epsilon Model Connectivity), que especifica uma 
API na qual os drivers das diferentes tecnologias de modelação são implementados. 
Os drivers EMC existem para tecnologias como EMF, MDR, UML e XML simples.
O epsilon possui uma variedade de tarefas específicas para diferentes operações de gestão de modelos. 

Por exemplo, o **EOL** (Epsilon Object Language) é usada para manipulação direta de modelos, incluindo a criação de novos modelos, consultas, atualizações e remoção de elementos de modelos. 

O **EOL** também forma o núcleo de outras linguagens no conjunto de ferramentas do Epsilon. 

Existem outras linguagens Epsilon nomeadamente: 

**EVL** (Epsilon Validation Language), para validar e criar regras nos modelos criados,

**ETL** (Epsilon Transformation Language), para transformação de modelo em modelo, 

**EGL** (Epsilon Generation Language), para transformação de modelo em texto, 

**Eugenia**, front-end para simplicar o desenvolvimento dos editores GMF,

**HUTN**, implementação de uma anotação textual, alternativa ao XText.

![example](img/tecnologias_epsilon.png)

Figura que demonstra a arquitetcura da tecnologia Epsilon.


## Cenário ##

Relativamente a este cenário, pensamos num problema com base na nossa experiência académica em que pudéssemos enquadrar um projeto não pensando logo na implementação mas começar com uma abordagem MDE com base em modelos.
Posto isto, pensamos numa **Plataforma de Sensores de Qualidade De Água**. 

Descrevendo um pouco o projeto seria:

Consumir valores de sensores vindos de um CSV nomeadamente, pH, NH3 e Cl2 que simulam a existência de sensores. Estes são facilmente interpretados pois iriam ser convertidos em  formato XML, formato standard, que permite a integração de sistemas tornando-o numa multiplataforma.

O objetivo seria criar uma aplicação que permita a validação dos valores de pH, NH3 e Cl2 previamente definidos pelo utilizador e que perante a definição de umas regras publicasse quais os valores fora da gama de um dado elemento, como se fosse um alarme, registando-os num XML. Os dados que se encontram dentro da gama esperada são considerados dados dentro dos parâmetros permitidos por lei, sendo registados noutro ficheiro XML.

Introduzindo o epsilon neste problema, através da tecnologia **EVL** ou **EGL**, tecnologias que permitem validar metamodelos ou XML, valida-se a estrutura e integridade dos dados da água num ficheiro XML onde nessas regras validaríamos se os valores encontram-se dentro da gama esperada. A titulo de exemplo o valor do pH esperado é entre 0 e 14, onde de 0 a 7 é considerado acido e 7 a 14 básico, na eventualidade de o valor do pH der 17 estamos num caso de uma situação de alarme.  

Por fim, após serem validados quais os valores em situação de alarme e quais 
os valores permitidos por lei poderia através do **EGL** 
transformar os valores registados em paginas html onde 
seja permitido ao utilizador métricas sobre os dados dos sensores 
e dos alarmes apresentados em forma de listagens e gráficos.

A titulo de exemplo demonstramos um XML com o formato de entrada que teria a nossa aplicação.
 
![example](img/CENARIO_1.png)

A nível de validação a partir do **EVL** poderíamos ter validações como por exemplo as enunciadas no exemplo seguinte. Ou seja, seria necessário converter as regras expostas no XML em formato **.EVL**, onde teríamos de definir regras para definir os valores máximos para o pH, NH3 e Cl2.
 
![example](img/CENARIO_2.png)

No final, teríamos dois ficheiros XML já validados, um com os alarmes e o outro com a dados dentro dos parâmetros permitidos. Nesta fase poderíamos aplicar a tecnologia EGL para transformar o XML em páginas HTML com um dashboard semelhante ao da figura seguinte:
 
![example](img/CENARIO_3.png)

Certamente para gerarmos um ficheiro com este formato teríamos de usar **EGX** para transformar os elementos do XML para elementos html. Assim como transformar o “dataSensorStore” para uma pagina índex.html, semelhante ao exemplo demonstrado no EGL.

Ou seja, no final teríamos 3 templates gerados:

•	Primeiro com o índex com as opções de “Alarmes” e “Data”;

•	Segundo template com os gráficos dos valores registados

•	Template final com os alarmes registados 

O resultado final para os valores que se encontram dentro dos valores esperados seria o seguinte:
 
![example](img/CENARIO_4.png)

E para a demonstração dos alarmes teria a seguinte estrutura:
 
![example](img/CENARIO_5.png)


Tendo em conta este exemplo podemos concluir que conseguiríamos endereçar os seguintes itens:

•	**Validação do modelo usando EVL**, neste caso do XML, uma vez que o Epsilon para alem dos metamodelos EMF também permite o XML, onde iriamos validar os valores dos nossos parâmetros pH, NH3 e Cl2.

•	**Transformação de M2M** ao passarmos de elementos XML para elementos HTML usando a tecnologia **EGX**.

•	**Transformação de M2T** usando **EGL** criando os templates que irão gerar o índex.html assim como o template para os alarmes e para os dados que se encontram dentro dos parâmetros permitidos por lei.

## Detalhes ##

Para facilitar a leitura dividimos um readme.md para cada tecnologia:

**Criação dos metamodelos - EOL**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](research-eol.md)

**Validações do Modelo Epsilon validation Language - EVL**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](research-evl.md)

**Tranformações Modelo-Modelo Epsilon Transformation Language - ETL**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](research-etl.md)

**Tranformaçôes Modelo-Texto Epsilon Generation Language - EGL**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](research-egl.md)

**DSL, Eugenia e HUTN**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](research-eugenia_hutn.md)

## Distribuição das tarefas ##

**Introdução & Cenário** - João Pinto

**Criação dos metamodelos - EOL** - Pedro

**Validações do Modelo Epsilon validation Language - EVL** - Lucas

**Tranformações Modelo-Modelo Epsilon Transformation Language - ETL** - João Santos

**Tranformaçôes Modelo-Texto Epsilon Generation Language - EGL** - João Pinto

**DSL, Eugenia e HUTN** - Pedro

## Conclusao ##

Com base neste trabalho constatamos que o Epsilon é uma tecnologia bastante poderosa no mundo dos MDEs 
uma vez que esta tecnologia contempla todas as fases desde a conceção do modelo até ao 
código final onde existe muita bibliografia assim como artigos demonstrando a utilidade desta tecnologia.
Epsilon pode ser uma boa alternativa às tecnologias que são lecionadas na disciplina de EDOM e tem uma grande
vantagem é que Epsilon para além de ser possivel EMF também é possivel usar XML como ficheiros de entrada.