# EDOM - Project Research Topic

In this folder you should add **all** artifacts developed for the investigative topic of the project.

There is only one folder for this task because it is a team task and, as such, usually there will be no need to create individual folders.

**Note:** If for some reason you need to bypass these guidelines please ask for directions with your teacher and **always** state the exceptions in your commits and issues in bitbucket.

---

# JaMoPP (Java Model Printer and Parser)

## 1. Introdução

Ao longo da história de todas as linguagens de programação ocorreu a introdução de novas funcionalidades na linguagem e ocasionalmente algumas foram removidas. No caso da linguagem Java tem um propósito de uso generalizado, ou seja, foram aplicadas novas extensões nos últimos anos e muitas mais serão planeadas no futuro. Frequentemente, essas extensões de linguagem fornecem meios para substituir construções complexas por outras mais compactas. Para se beneficiar das novas extensões da linguagem para grandes corpos do código fonte existente, é necessária uma técnica que execute a modernização do código fonte existente automaticamente.

As **transformações do código fonte** já são conhecidas há bastante tempo e existem ferramentas especializadas para executar esta tarefa. No entanto, com o **Model-Driven Software Development (MDSD)**, linguagens de transformação padronizadas (por exemplo, Query View Transformation (QVT) ficaram disponíveis. Se fosse possível usar essas linguagens para transformação de código, a necessidade de linguagens especializadas desapareceria. Para aplicar uma linguagem de transformação de modelo ao código-fonte, é necessário um modelo do respetivo código. Além disso, é necessário um metamodelo da linguagem que está sujeito a transformação. Aqui é que entra o **JaMoPP (Java Model Printer and Parser)**, uma ferramenta que envolve um metamodelo completo para Java e ferramentas para converter o código-fonte Java nos modelos Eclipse Modeling Framework (EMF) e vice-versa. Portanto, o JaMoPP permite que ferramentas arbitrárias baseadas em EMF funcionem em programas Java.

Com esta pesquisa iremos tentar mostrar como o JaMoPP e uma linguagem de transformação de modelo padronizada podem ser combinados para gerar o código Java. 

---

## 2. MODEL-DRIVEN SOFTWARE DEVELOPMENT (MDSD)

O Model-Driven Software Development é baseado em modelos padronizados que são refinados, transformados e eventualmente traduzidos em código executável usando geradores de código. O objetivo do MDSD (Model-Driven Software Development) é a geração (semi-) automática de sistemas de software a partir de modelos em vários estados. Ou seja, não é apenas gerado apenas o código-fonte a partir de modelos, mas também os modelos são transformados e refinados em relação a outros modelos. Usando uma linguagem padronizada, ferramentas independentes de linguagem podem ser utiliadas para manipular e analisar modelos definidos em diferentes linguagens de modelagem. Como um metamodelo define tipos e restrições para sentenças (ou seja, modelos) de uma linguagem, todas as manipulações de modelos feitas por diferentes ferramentas podem ser verificadas quanto à correção.

---

## 3. JAVA MODEL PRINTER AND PARSER (JaMoPP)

No MDSD, existem muitas ferramentas de modelagem genérica que podem ser usadas em combinação com linguagens arbitrárias. Isso é possível porque as ferramentas podem ser configuradas com metamodelos. Um metamodelo descreve os conceitos de uma linguagem, por outras palavras, também chamada de sintaxe abstrata da uma linguagem. Para trocar metamodelos entre ferramentas, o OMG padronizou as linguagens de metamodelo Meta-Object Facility (MOF) e Essential Meta-Object Facility (EMOF). Uma implementação amplamente usada do EMOF é o Ecore como parte do EMF. Para usar uma ferramenta de modelagem genérica que suporta o Ecore com uma determinada linguagem, um metamodelo dessa linguagem precisa ser fornecido juntamente com ferramentas para analisar (e imprimir) frases escritas na sintaxe concreta da linguagem (por exemplo, um programa Java) em gráficos digitados que em conformidade com o metamodelo. Com o JaMoPP, fornecemos esse metamodelo Ecore e as ferramentas para analisar e imprimir o código fonte da linguagem Java. Isso permite que os desenvolvedores apliquem ferramentas de modelagem genérica no código-fonte Java e, portanto, usem as mesmas ferramentas para trabalhar com modelos (por exemplo, modelos UML) e código-fonte.

As propriedades importantes do JaMoPP são: 

* O JaMoPP suporta análise e impressão de código Java, o que permite que as ferramentas de modelagem (por exemplo, mecanismos de transformação de modelo) possam ler e modificar o código-fonte Java. Essa conversão preserva o layout do código-fonte Java.

* Além de analisar, o JaMoPP realiza uma análise de nome e tipo do código-fonte e estabelece links (por exemplo, entre o uso e a definição de uma classe Java). Esses links podem ser explorados por ferramentas de modelagem para garantir a correção das propriedades da semântica estática dos arquivos de origem Java que eles geram ou modificam. 

* O próprio JaMoPP foi desenvolvido utilizando a ferramenta de modelagem EMFText. Lá, a sintaxe concreta de Java é definida em uma definição de sintaxe EBNFlike. Com base no metamodelo e nesta definição, o analisador e as ferramentas da impressora são gerados. Isso nos permite estender o JaMoPP estendendo o metamodelo e a definição de sintaxe sem a necessidade de modificar o código.

O JaMoPP foi testado com um grande corpo de código-fonte em projetos Java de código aberto, através dos quais a estabilidade e o suporte a todos os recursos da linguagem Java 5 são garantidos.

---

## 4. Projecto exemplo

O  JaMoPP pode ser usado em conjunto com o eclipse usando um plugin. Para por em pratica esta ferramenta foi usado o projeto exemplo fornecido na documentação. Neste projeto é utilizado uma transformação ATL em conjunto com o plugin para converter um diagrama de classes em UML para código java.

---

## 5. Referências

1. https://www.researchgate.net/publication/216005110_Model-driven_Modernisation_of_Java_Programs_with_JaMoPP
2. https://github.com/DevBoost/JaMoPP
3. https://github.com/DevBoost/JaMoPP/blob/master/Doc/org.emftext.language.java.doc/publications/2009_SLE_JaMoPP.pdf
4. https://github.com/DevBoost/JaMoPP/blob/master/Doc/org.emftext.language.java.doc/publications/2009_REM_JaMoPPReverse.pdf
5. https://github.com/DevBoost/JaMoPP/blob/master/Doc/org.emftext.language.java.doc/publications/2011_MDSM_JaMoPPModernise.pdf
6. https://github.com/DevBoost/JaMoPP/tree/master/Examples/org.emftext.language.java.examples
