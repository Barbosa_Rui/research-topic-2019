/**
 */
package pt.isep.edom.mindstorms;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Release</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pt.isep.edom.mindstorms.MindstormsPackage#getRelease()
 * @model
 * @generated
 */
public interface Release extends Action {
} // Release
