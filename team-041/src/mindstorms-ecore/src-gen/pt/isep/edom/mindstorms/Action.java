/**
 */
package pt.isep.edom.mindstorms;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pt.isep.edom.mindstorms.MindstormsPackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends Instruction {
} // Action
